# Docker Web Service with Apache and Traefik

- This projects makes use of Apache and Traefik to create a webserver
- The project is run within a docker container and docker compose is utilised for running multi containers (i.e. webserver container and Traefik container)
- Port `80` and `443` is exposed and all unsecure requests to the server is redirected to the secure route
- TLS version used is > 1.1. 

## Requirements
- Docker installed `latest`
- Mac OS (or Linux OS)

## Run Test cases
### Navigate to project root directory 
- run `$ cd dockerwebservice`
### Run `sh` file
- run `$ sh Test.sh`
### Expected result 
` starting docker containers.....`

` [+] Running 2/2`

`  ⠿ Container dockerwebservice_webserver_1  Started                          2.4s`

` ⠿ Container dockerwebservice_traefik_1    Started                         2.4s`

` waiting 5 secs for container to initialize....`

` Testing HTTP on port 80........`

- `$ true`

` Testing HTTPS on port 443........`

- `$ true`

` Testing HTTPS on port 80........`

- `$ false`

`stopping docker containers.....`


## Run Project
### Navigate to project root directory 
- run `$ cd dockerwebservice`
### Create and run containers
- run `$ docker compose up -d`
### Check to see containers are up 
- run `$ docker container ls`