#!/bin/zsh
echo "\n starting docker containers.....\n"
docker compose up -d
echo "\n waiting 5 secs for container to initialize....\n"
sleep 5
echo "\n Testing HTTP on port 80........\n"
curl -silent --output /dev/null http://127.0.0.1:80
if [ $? -eq 0 ]; then
    echo true
else
    echo false
fi
sleep 1
echo "\n Testing HTTPS on port 443........\n"
curl -k -silent --output /dev/null https://127.0.0.1:443
if [ $? -eq 0 ]; then
    echo true
else
    echo false
fi
sleep 1
echo "\n Testing HTTPS on port 80........\n"
curl --max-redirs 0 -silent --output /dev/null https://127.0.0.1:80
if [ $? -eq 0 ]; then
    echo true
else
    echo false
fi

echo "stopping docker containers.....\n"
docker stop dockerwebservice_traefik_1
sleep 3
docker stop dockerwebservice_webserver_1 


# echo "Testing HTTP port 80"
# if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
#   echo true
# else
#   echo false

# echo "Testing HTTPS port 443"
# if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
#   echo true
# else
#   echo false

# echo "Testing HTTPS port 80"
# if ping -q -c 1 -W 1 8.8.8.8 >/dev/null; then
#   echo true
# else
#   echo false

# fi
